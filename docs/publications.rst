.. _publications:

Publications
============

The following publications were enabled, to some extent, by WrightTools.
The authors of these publications have volunteered to appear on this page.

Would you like your publication to appear here?
Email a developer---or better yet, make a pull request.

Ordered by date of publication, newest first.

#. **Group- and phase-velocity mismatch fringes in triple sum-frequency spectroscopy**
   Darien J. Morrow, Daniel D. Kohler, and John C. Wright
   *Phys. Rev. A* **2017** 96 (6)
   `doi:10.1103/PhysRevA.96.063835 <https://doi.org/10.1103/PhysRevA.96.063835>`_

#. **Multidimensional Spectral Fingerprints of a New Family of Coherent Analytical Spectroscopies**
   Nathan Andrew Neff-Mallon and John C. Wright
   *Anal. Chem.* **2017** 89, 24, 13182--12189
   `doi:10.1021/acs.analchem.7b02917 <https://doi.org/10.1021/acs.analchem.7b02917>`_
   
#. **Frequency-domain coherent multidimensional spectroscopy when dephasing rivals pulsewidth:
   Disentangling material and instrument response**
   Daniel D. Kohler, Blaise J. Thompson, and John C. Wright
   *J. CHem. Phys* **2017** 147, 084202
   `doi:10.1063/1.4986069 <https://doi.org/10.1063/1.4986069>`_
